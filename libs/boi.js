var converter = require('./converter'),
    baseUrl = "http://abe.api.usno.navy.mil/rstt/oneday?",
    apiUrl = baseUrl,
    cycle = ["ty", "suu", "dan", "mao", "thin", "ti",
            "ngo","mui", "than", "dau", "tuat", "hoi", "ty"],
    cycle2 = ["hoi","ty", "suu", "dan", "mao", "thin", "ti",
            "ngo","mui", "than", "dau", "tuat", "hoi"];
var longitude, latitude, localeDate, localeOffset;
const CauEnum = {
    CAU_TAI: 1,
    XUAT_HANH: 2,
    HANH_NHAN: 3,
    GIAO_DICH: 4,
    QUAN_SU: 5,
    BENH_TRONG: 6,
    KINH_DOANH: 7,
    SINH_SAN: 8,
    THI_PHI: 9
}

function resetApiUrl() {
    apiUrl = baseUrl
}

function thienCuong(cung) {
    var index = cycle.indexOf(cung), 
        cung;
    if (index < 0) {
        cung = `No such element ${cung}`;
    } else {
        index = index? index-1 : 11
        cung = cycle[index]
    }
    return {cung, index};
};
function tinhManhQuyTrong(index) {
    switch (index%3) {
        case 0:
            return "trong";  // ty ngo mao dau
        case 1: 
            return "quy"; // thin tuat suu mui
        case 2:
            return "manh"; //dan than ti hoi
        default:
            return "Something went wrong tinh MANH TRONG QUY";
    }
}

function getTimeNow() {
    var now = new Date(); 
    localeOffset = now.getTimezoneOffset() / -60
    localeDate = now.toLocaleDateString('en-US',  { timeZone: "Australia/Melbourne" })

    var [month, day, year] = localeDate.split(/[\/\\]/) ,
        options = { timeZone: "Australia/Melbourne", hour12: false, hour: '2-digit', minute:'2-digit'},
        gio = now.toLocaleTimeString('en-US', options);
    var thangIndex =  {1: "jan", 2: "feb", 3: "mar", 4: "apr", 5: "may", 6: "jun", 7: "jul",
                        8: "aug", 9: "sep", 10: "oct", 11: "nov", 12: "dec"},
        [ngay, mon] = converter.convertSolar2Lunar(Number(day), Number(month), Number(year), localeOffset),
        thang = thangIndex[mon];
    return {ngay, thang, gio};
}

function tinhGioAmByCoords(gio) {
    var timeArray, index
    apiUrl += `&coords=${latitude},${longitude}&date=${localeDate}&tz=${localeOffset}`
    console.log(apiUrl)
    return fetch(apiUrl)
        .then(response => response.json())
        .then( (responseData) => { 
            let upperTransit = responseData.sundata.find((phenonmemon) => { return phenonmemon.phen === 'U'}).time
            let [hh, mm] = upperTransit.split(":")
            if (`${hh-1}:${mm}` < "12:00") {
                timeArray = ["00:00",`01:${mm}`,`03:${mm}`,`05:${mm}`,`07:${mm}`,`09:${mm}`,`11:${mm}`,`13:${mm}`,`15:${mm}`,`17:${mm}`,`19:${mm}`,`21:${mm}`,`23:${mm}`]
                index = findCanhGio(timeArray, gio);
                console.log("First condition: " + index)
                return {cung: cycle[index], index};
            } else if (`${hh-1}:${mm}` > "12:00") {
                timeArray = ["00:00",`00:${mm}`,`02:${mm}`,`04:${mm}`,`06:${mm}`,`08:${mm}`,`10:${mm}`,`12:${mm}`,`14:${mm}`,`16:${mm}`,`18:${mm}`,`20:${mm}`,`22:${mm}`,`24:01`];
                index = findCanhGio(timeArray, gio);
                console.log("Second condition: " + index)
                return {cung: cycle2[index], index: cycle.indexOf(cycle2[index])};
            } else {
                timeArray = ["00:00","02:00","04:00","06:00","08:00","10:00","12:00","14:00","16:00","18:00","20:00","22:00"];
                index = findCanhGio(timeArray, gio);
                console.log("Third condition: " + index)
                return {cung: cycle[index], index};
            }
         })
        .catch( (error) => { 
            console.log(error) 
            throw new Error("Error retrieved from time api")
        });
}


function tinhCung({ngay, thang, gio}) {
    var thangCycle = {jan: "suu", feb: "ty", mar: "hoi", apr: "tuat", may: "dau", jun: "tuat",
                    jul: "hoi", aug: "ty", sep: "suu", oct: "dan", nov: "mao", dec: "dan"};
    var cung = thangCycle[thang];
    var index = cycle.indexOf(cung)
    
    index = (index + ngay) % 12 - 1
    if (index == -1) {
        index = 11
    }
    console.log(gio)
    var gioAm
    tinhGioAmByCoords(gio)
    .then((value) => {  
        console.log(value)
        gioAm = value.index
        index = (index + gioAm) % 12
        console.log(index)
    }).catch( error => { 
        console.log("Time is not calculated by Coordinates. Using other method")
        console.log (thang + " " + gio )
        gioAm = tinhGioAmLich({thang, gio}).index
        index = (index + gioAm) % 12
        console.log(index)
    })

    return cycle[index]
}

function findCanhGio(timeArray, time, low, high) {
    if (typeof(low) == 'undefined') {
        low = 0;
    } 
    if (typeof(high) == 'undefined') {
        high = timeArray.length - 1;
    }
    var mid = Math.floor((low + high) / 2);
    if (time >= timeArray[mid] && time < timeArray[mid+1] ) {
        return mid;
    } else if (time < timeArray[mid]) {
        high = mid-1;
    } else if (time > timeArray[mid]) {
        low = mid+1;
    } 
    if (low <= high ) {
        return findCanhGio(timeArray, time, low, high);
    } else {
        return mid;
    } 
}

function setLocation(lat, long) {
    latitude = lat
    longitude = long
}

function tinhGioAmLich(time) {
    var timeArray,
        index;
    switch (time.thang) {
        case "jan":
        case "sep":
            timeArray = ["00:00", "01:30", "03:30", "05:30", "07:30", "09:30", "11:30", "13:30", "15:30", "17:30", "19:30", "21:30", "23:30"];
            index = findCanhGio(timeArray, time.gio);
            return {cung: cycle[index], index};
        case "mar":
        case "jul":
            timeArray = ["00:00", "01:50", "03:50", "05:30", "07:50", "09:50", "11:50", "13:50", "15:50", "17:50", "19:50", "21:50", "23:50"];
            index = findCanhGio(timeArray, time.gio);
            return {cung: cycle[index], index};
        case "may":
            timeArray = ["00:00", "00:10", "02:10", "04:10", "06:10", "08:10", "10:10", "12:10", "14:10", "16:10", "18:10", "20:10", "22:10", "24:01"];
            index = findCanhGio(timeArray, time.gio);
            return {cung: cycle2[index], index: cycle.indexOf(cycle2[index])};
        case "feb":
        case "aug":
        case "oct":
        case "dec":
            timeArray = ["00:00", "1:40", "03:40", "05:40", "07:40", "09:40", "11:40", "13:40", "15:40", "17:40", "19:40", "21:40", "23:40"];
            index = findCanhGio(timeArray, time.gio);
            return {cung: cycle[index], index};
        case "apr":
        case "jun":
            timeArray = ["00:00", "02:00", "04:00", "06:00", "08:00", "10:00", "12:00", "14:00", "16:00", "18:00", "20:00", "22:00"];
            index = findCanhGio(timeArray, time.gio);
            return {cung: cycle[index], index};
        case "nov":
            timeArray = ["00:00", "01:10", "03:10", "05:10", "07:10", "09:10", "11:10", "13:10", "15:10", "17:10", "19:10", "21:10", "23:10"];
            index = findCanhGio(timeArray, time.gio);
            return {cung: cycle[index], index};
        default:
            return "Not a valid month";
    }
}

function boiDonThienCuong (yCau) {
    var index = thienCuong(tinhCung(getTimeNow())).index,
        mtq = tinhManhQuyTrong(index),
        queBoi, loiChu, loiGiai;
    switch (yCau) {
        case CauEnum.CAU_TAI:
            loiChu = { manh: `Mạnh thì cầu tài khó được`, trong: `Trọng thì cầu tài được nhưng chậm`, quy: `Quý thì có ngay Tài từ hướng Đông đưa đến`}
            queBoi = `Thiên Cương gia Mạnh cầu nan đắc\nGia Mạnh cầu đắc dã tu trì\nGia Quý chi thời cầu lập đắc\nTài hướng Đông lai cánh vô nghi \n\n`
            break;
        case CauEnum.XUAT_HANH:
            queBoi = `Thiên Cương gia Mạnh đại cát xương\nGia Trọng xuất hành vi họa ương\nGia Quý chi thời đa bình ổn \nQuân lưu y thử định hà phương \n\n`
            loiChu = { manh: "Mạnh là rất tốt", trong: "Trọng thì không nên,đi ra e có tai họa đến thân", quy: "Quý bình an" }
            break;
        case CauEnum.HANH_NHAN:
            queBoi = `Thiên Cương gia Mạnh thân bất động \nGia Trọng chi thời bán lộ lai \nGia Quý chi thời tức cánh chí \nQuân hành tu ký thử tam thời. \n\n`
            loiChu = { manh: "Mạnh thì người chưa đi", trong: "Trọng thì người đã đi được nửa đường ", quy: "Quý thì người sắp đến nơi , chừng khoảng 3 canh giờ thì đến."}
            break;
        case CauEnum.GIAO_DICH:
            queBoi = `Thiên cương gia Mạnh ứng bất toại \nGia Trọng chi thời ứng thành khả mưu \nGia Quý chi thời khả toại ý \nTrở trệ ứng vô mạc tu sầu. \n\n`
            loiChu = { manh: "Mạnh thì không được ", trong: "Trọng thì có thể ứng thành ", quy: "Quý thì toại ý , lúc đầu có trắc trở , đình trệ xin chớ lo , mọi việc trước trì trệ sau hanh thông " }
            break;
        case CauEnum.QUAN_SU:
            queBoi = `Nhược vấn quan sự cát dữ hung \nGia Mạnh vô lý nhi hòa bình \nGia Trọng tha thâu thử tiếu bĩ \nQuý thượng yêm lưu ngã tất doanh. \n\n`
            loiChu = { manh: "Mạnh e không thể an bình", trong: "Trọng thì đắc lợi ", quy: "Quý thời thắng kiện , đắc lý "}
            break;
        case CauEnum.BENH_TRONG:
            queBoi = `Thiên Cương gia Mạnh nhân bệnh trọng \nGia Trọng bệnh khinh bất dụng sầu \nGia Quý chi thời nan đắc hảo \nCấp nghi nhương tạ tảo đồ mưu \n\n`
            loiChu = { manh: "Mạnh thì bệnh nặng ", trong: "Trọng thì bệnh nhẹ , không cần sầu lo ", quy: "Quý thì không tốt , phải mau cúng vái cầu xin mới mong qua khỏi. " }
            break;
        case CauEnum.KINH_DOANH:
            queBoi = `Kinh doanh lợi ích hữu kiêm vô \nGia Mạnh nan cầu ý bất như \nGia Trọng bình bình , Quý mãn ý \nQuân năng y thử định vô nghi. \n\n`
            loiChu = {manh:  "Mạnh thì không như ý", trong: "Trọng thì buôn bán bình thường ", quy: "Quý thì buôn may bán đắt."};
            break;
        case CauEnum.SINH_SAN:
            loiChu = { manh: "Mạnh thì là sanh trai", trong: "Trọng thì sanh nữ", quy: "Quý thì người mẹ nguy hiểm , phải mau làm Phúc mới mong cứu được." }
            queBoi = `Thiên Cương gia Mạnh thị nam tử \nGia Trọng nữ nhơn định thị nghi \nGia Quý chi thời sản phụ tử \nCấp nghi tác Phúc đắc an ninh. \n\n`
            break;
        case CauEnum.THI_PHI:
            var extra = `\nĐây là phép tìm phương tránh những chuyện thị phi ngày xưa vậy. NCD chỉ biên ra đây , còn ngày nay gặp chuyện chúng ta cần đương đầu tìm cách giải quyết , khác với xưa vậy.`
            loiChu = { manh: `Mạnh thì tránh về hướng Đông ${extra}`, trong: `Trọng thì tránh về hướng Tây ${extra}`, quy: `Quý thì tránh về Đông Bắc. ${extra}`}
            queBoi = `Hồi tị chi nhân thị dữ phi \nMạnh Đông vi cát , Trọng nghi Tây \nAn chi Đông Bắc gia chi Quý \nThử thị thần tiên diệu tích ky \n\n`
            break;
        default:
            queBoi = ""
            loiGiai = ""
            return {queBoi, loiGiai};
    }
    loiGiai = loiChu[mtq]
    return {queBoi, loiGiai}    
}


module.exports = {
    title: "Boi",
    thienCuong: thienCuong,
    tinhGioAmLich: tinhGioAmLich,
    tinhCung: tinhCung,
    tinhManhQuyTrong: tinhManhQuyTrong,
    boiDonThienCuong: boiDonThienCuong,
    setLocation: setLocation,
    tinhGioAmByCoords: tinhGioAmByCoords,
    resetApiUrl
}; 

