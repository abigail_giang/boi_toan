describe("Boi", function() {
    var boi = require("../node_modules/src/boi");
    describe("Thien cuong", function() {
        it("Thien Cuong suu is ty", function(){
        expect(boi.thienCuong("suu").cung).toEqual("ty");
        });
        it("Thien Cuong ty is hoi", function(){
            expect(boi.thienCuong("ty").cung).toEqual("hoi");
        });
        it("Thien Cuong unknown shows error", function(){
            expect(boi.thienCuong("goblin").cung).toEqual("No such element goblin");
        })
    })
    
    describe("Chuyen sang ngay gio am lich", function(){
        it("thang 1 , 07:29", function(){
            expect(boi.tinhGioAmLich({thang: "jan", gio:"07:29"}).cung).toEqual("mao");
        })
        it("thang 1, 05:29", function(){
            expect(boi.tinhGioAmLich({thang: "jan", gio:"05:29"}).cung).toEqual("dan");
        })
        it("thang 1, 07:30", function(){
            expect(boi.tinhGioAmLich({thang: "jan", gio:"07:30"}).cung).toEqual("thin");
        })
        it("thang 1, 07:35", function(){
            expect(boi.tinhGioAmLich({thang: "jan", gio:"07:35"}).cung).toEqual("thin");
        })
        it("thang 9, 23:40", function(){
            expect(boi.tinhGioAmLich({thang: "sep", gio:"23:40"}).cung).toEqual("ty");
        })
        it("thang 9, 01:20", function(){
            expect(boi.tinhGioAmLich({thang: "sep", gio:"01:20"}).cung).toEqual("ty");
        })
        it("thang 5, 00:05", function(){
            expect(boi.tinhGioAmLich({thang: "may", gio:"00:05"}).cung).toEqual("hoi");
        })
        it("thang 5, 24:00", function(){
            expect(boi.tinhGioAmLich({thang: "may", gio:"24:00"}).cung).toEqual("hoi");
        })
        it("thang 5, 14:15", function(){
            expect(boi.tinhGioAmLich({thang: "may", gio:"14:15"}).cung).toEqual("mui");
        })
        it("thang 5, 22:10", function(){
            expect(boi.tinhGioAmLich({thang: "may", gio:"22:10"}).cung).toEqual("hoi");
        })
        it("thang 5, 13:10", function(){
            expect(boi.tinhGioAmLich({thang: "may", gio:"13:10"}).cung).toEqual("ngo");
        })
        it("thang 2, 01:59", function(){
            expect(boi.tinhGioAmLich({thang: "feb", gio:"01:59"}).cung).toEqual("ty");
        })
        it("thang 2, 23:00", function(){
            expect(boi.tinhGioAmLich({thang: "feb", gio:"23:00"}).cung).toEqual("hoi");
        })
        it ("thang 12, gio 06:30", function() {
            expect (boi.tinhGioAmLich({thang:'dec', gio: "06:30"}).cung).toEqual("mao")
        })
        it("thang 4, 23:59", function(){
            expect(boi.tinhGioAmLich({thang: "apr", gio:"23:59"}).cung).toEqual("hoi");
        })
        it("thang 4, 23:59", function(){
            expect(boi.tinhGioAmLich({thang: "apr", gio:"23:59"}).cung).toEqual("hoi");
        })
        it("thang 11, 23:40", function(){
            expect(boi.tinhGioAmLich({thang: "nov", gio:"23:40"}).cung).toEqual("ty");
        })
        it ("thang 5 ngay 8 gio ty", function() {
            expect (boi.tinhCung({thang: 'may', ngay: 8, gio: "02:15"})).toEqual("thin")
        })
        it ("thang 11 ngay 14 gio ty", function() {
            expect (boi.tinhCung({thang: 'nov', ngay: 14, gio: "01:00"})).toEqual("thin")
        })
        it ("thang 11 ngay 14 gio tuat", function() {
            expect (boi.tinhCung({thang: 'nov', ngay: 14, gio: "19:30"})).toEqual("dan")
        })
        it ("thang 12 ngay 13 gio mao", function() {
            expect (boi.tinhCung({thang: 'dec', ngay: 13, gio: "06:30"})).toEqual("ti")     
        })
        it ("thang 12 ngay 1 gio hoi", function() {
            expect (boi.tinhCung({thang: 'dec', ngay: 1, gio: "23:39"})).toEqual("suu")
        })

        it ("ty is TRONG", function() {
            var index = boi.thienCuong("suu").index
            expect (boi.tinhManhQuyTrong(index)).toEqual("trong")
        })
        it ("suu is QUY", function() {
            var index = boi.thienCuong("dan").index
            expect (boi.tinhManhQuyTrong(index)).toEqual("quy")
        })
        it ("dan is MANH", function() {
            var index = boi.thienCuong("mao").index
            console.log(index);
            expect (boi.tinhManhQuyTrong(index)).toEqual("manh")
        })
    })
});