import React from 'react';
import { Picker, Button, Alert, StyleSheet, Text, View, Modal, TouchableHighlight } from 'react-native';
import { Component } from 'react';
import boi from '../../libs/boi';

export class Blink extends Component {
  constructor(props) {
    super(props);
    this.state = { showText: true };
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.blink(), 1000);
  }    
  componentWillUnmount() { //tear down timer 
    clearInterval(this.timerID);
  }

  blink() {
    this.setState(previousState => {
      return { showText: !previousState.showText };
    });
  }

  render() {
    let display = this.state.showText ? this.props.text : ' ';
    return (
      <Text>{display}</Text>
    );
  }
}

export class LoiGiai extends Component {
  constructor(props) {
    super(props);
    this.state= {queBoi:" ", loiGiai:" "};    
  }
  componentDidMount() { //after component has been rendered by the DOM
    this.timerID = setInterval(() => this.tick(), 1000);
  }
  componentWillUnmount() { //tear down timer 
    clearInterval(this.timerID);
  }

  tick() {
    this.setState(result)
  }
  render() {
    return (
      <View>
        <Text>{this.state.queBoi}</Text>        
        <Text>{this.state.loiGiai}</Text>
      </View>
    );
  }
} 

export class Geolocation extends Component {
  constructor (props) {
    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      error: null
    };
  }

  componentDidMount() {
    this.updateLocation()
  }

  updateLocation() {
    navigator.geolocation.getCurrentPosition(
      position => {
        latitude = position.coords.latitude.toFixed(2);
        longitude = position.coords.longitude.toFixed(2);
        if (latitude && longitude) {
          this.setState({
            latitude: latitude,
            longitude: longitude,
            error: null
          }, function() {
            boi.setLocation(this.state.latitude, this.state.longitude);
          });
        }
        
      },
      error => this.setState({ error: error.message}),
      { enableHighAccuracy: false, timeout: 5000, maximumAge: 1000},
    ); 
    
    navigator.geolocation.stopObserving()
  }

  componentWillUnmount() {
    navigator.geolocation.stopObserving()
  }

  render() {
    return (
      <View style={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Latitude: {this.state.latitude} & Longitude: {this.state.longitude}</Text>
        {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
        <Button onPress={ () => { this.updateLocation();  }} 
          title="Kinh do, Vi do" />
      </View>
    )
  }
}

export class Buttons extends Component {
  constructor (props) {
    super (props)
  }

  render() {
    return (
      <View style={{flex: 1, flexDirection: 'row',  justifyContent: 'center', marginTop:200}}>
        <View style={{width: 80, height: 30, backgroundColor: 'powderblue'}}>
          <Button 
            onPress={() => {
              result = boi.boiDonThienCuong(Number(this.props.yCau))
              this.props.boiState()
            }}
            title = {boi.title}   
          />
        </View>
        <View style={{width: 80, height: 30, backgroundColor: 'skyblue'}}>
          <Button 
            onPress={() => {
              result = {queBoi:" ", loiGiai:" "}
              this.props.resetState()
              boi.resetApiUrl()
            }}
            title = "Reset"
          />
        </View>
        <View style={{width: 80, height: 30, backgroundColor: 'steelblue'}}>
            <Text>Empty field</Text>
        </View>
      </View>
    );
  }
  
}
