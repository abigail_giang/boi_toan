import React, { Component } from 'react'
import { Easing, Animated, Text, View, Platform, TouchableHighlight, PickerIOS, Dimensions } from 'react-native'


export class FadeInView extends Component {
    state = { 
        fadeAnim: new Animated.Value(0),
        position: new Animated.ValueXY({x:0, y: 0}),
        twirl: new Animated.Value(0)
    }
    
    componentDidMount() {
        this.animatedSequence()
    }

    fadeIn() {
        Animated.timing(
            this.state.fadeAnim, {
                toValue: 1,
                duration: 10000
            }
        ).start()
    }

    animatedSequence() {
        Animated.sequence([            // decay, then spring to start and twirl
            Animated.decay(this.state.position, {   // coast to a stop
                velocity: {x: 15, y: 15}, // velocity from gesture release
                deceleration: 0.997
            }),
            Animated.parallel([          // after decay, in parallel:
                Animated.spring(this.state.position, {
                    toValue: {x: 10, y: 30},
                    overshootClamping: false,
                    restDisplacementThreshold: true,
                    restSpeedThreshold: true,
                    velocity: {x: 5, y: 5},
                    bounciness: 8,
                    speed: 2,
                    //tension: 2,
                    //friction: 3
                })
            ]),
        ]).start();                    // start the sequence group
    }

    render() {
        let { fadeAnim } = this.state;
        return (
            //<Animated.View style = {{ ...this.props.style, top: this.state.position.y, right: this.state.position.x }}></Animated.View>
            <Animated.Image
                style={{ width:100, height: 100, marginBottom: 30, transform: [{translateX: this.state.position.x}, {translateY: this.state.position.y}] }}
                source= {require("../../resources/img/tweetbird-icon.png")} />
                //source= {{ uri: '/Users/abigailn/Documents/Mine/My_App/boi-app/BoiProject/resources/img/tweetbird-icon.png' }} />
        )
    }
}
var PickerComponent
var PickerItem
if (Platform.OS === 'ios') {
    PickerComponent = PickerIOS
    PickerItem = PickerIOS.Item
} else {
    PickerComponent = Picker
    PickerItem = Picker.Item
}

var PickerIOSItem = PickerIOS.Item
var ynguyen = 
{
    1: "Cầu Tài",
    2: "Xuất Hành",
    3: "Hành Nhân",
    4: "Giao Dịch"
}

export class PickerAnimated extends Component 
{
    constructor (props) {
        super (props)
        let {deviceWidth, deviceHeight} = Dimensions.get('window')
        this.props.offSet = new Animated.Value(deviceHeight)
    }
    componentDidMount()
    {
        Animated.offSet(this.props.offSet, {
            duration: 300,
            toValue: 100
        }).start()
    }
    closeModal()
    {
        Animated.timing(this.props.offSet, {
            duration: 300,
            toValue: deviceHeight
        }).start(this.props.closeModal) 
    }
    render() 
    {
       return (
        <Animated.View style={{ transform: [{translateY: this.props.offSet}] }}>
            <View>
                <TouchableHighlight onPress={ this.closeModal } underlayColor="transparent">
                    <Text>Close</Text>
                </TouchableHighlight>
            </View>
            <PickerIOS>
                {Object.keys(ynguyen).map(y => (
                    <PickerIOSItem
                        key={y}
                        label={ynguyen[y]} 
                        value={y}/>
                ))}
            </PickerIOS>
        </Animated.View>
       )
   }
}
