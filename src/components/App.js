import { Button,PickerIOS, Picker, StyleSheet, Text, View, Modal, TouchableHighlight, Animated, Dimensions } from 'react-native';
import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Blink, LoiGiai, Geolocation, Buttons } from './Elements'
import { FadeInView, PickerAnimated } from './Animation'

export default class App extends Component {
  constructor () {
    super()
    this.state = { 
      yCau: "1", 
      pickerVisible: false, 
      mainVisible: true, 
      opacity: new Animated.Value(0)
    };
  }
  _setMainVisible(visible) 
  {
    this.setState({
      mainVisible: visible,
    })
  }
  _showPickerAnimation() 
  {
    console.log("Animation starts");
    let {height, width} = Dimensions.get('window')

    return Animated.timing(this.state.opacity, {
      toValue: 1,
      duration: 3000,
      useNativeDriver: true
    })   
     
  }
  render() {
    let { height, width } = Dimensions.get('window');
    let { opacity } = this.state

    let items = 
    {
      1: "Cầu Tài",
      2: "Xuất Hành",
      3: "Hành Nhân",
      4: "Giao Dịch",
      5: "Quan Sự",
      6: "Bệnh Trọng",
      7: "Kinh Doanh",
      8: "Sinh Sản",
      9: "Thị Phi"
    }
    return (
      <View style={[styles.container]}>
        <Modal visible= {this.state.mainVisible}
              animationType="slide"
              onRequestClose={()=>{}}>
          <View style={{flex:1, flexDirection: 'column', justifyContent: 'space-between', marginTop: 50}}>
            <View style={[styles.flex2, {alignItems: 'center'}]}> 
              <Text style= {styles.bigblue}>BÓI ĐỘN THIÊN CƯƠNG</Text>
              <Geolocation ></Geolocation>
              <TouchableHighlight onPress={() => this._showPickerAnimation().start()} underlayColor="transparent" >
                <Text style={{color: '#119114'}}>"Bạn muốn cầu gì?"</Text>
              </TouchableHighlight>
            </View>
            <View style={styles.flex2}>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <FadeInView style={{ width:250, height:50}} ></FadeInView>
              </View>
              <Animated.View
                style={{ 
                  opacity,
                  transform: [{ 
                    translateY: opacity.interpolate({
                      inputRange: [0, 1],
                      outputRange: [-50, 0]
                    }) 
                  }],
                }}>
                <View style={styles.toolbar}>
                  <View style={styles.toolbarRight}>
                    <Button title="Done" onPress={() => {}} />
                  </View>
                </View>
                <View style={{ height: 10 }}>

                <Picker
                  style={{ backgroundColor: '#e1e1e1', height: 100}}
                  itemStyle={{ fontSize: 16, height: 100, paddingVertical: 3 }}
                  selectedValue={this.state.yCau}
                  onValueChange={(itemValue, itemIndex) => this.setState({yCau: itemValue})}>
                  {/* <Picker.Item label="Cầu Tài" value="1" />
                  <Picker.Item label="Thị Phi" value="9" /> */}
                  {Object.keys(items).map(i => (
                    <Picker.Item
                      key={i}
                      label={items[i]}
                      value={i} />
                  ))}
                </Picker> 
                </View>

              </Animated.View>
            </View>
            <Buttons 
              yCau = {this.state.yCau} 
              resetState = {()=>{}} 
              boiState = {() => this._setMainVisible(!this.state.mainVisible)}>
            </Buttons>
          </View>
        </Modal>
        <Modal visible= {!this.state.mainVisible}
              animationType="slide"
              onRequestClose={()=>{this._setMainVisible(true)}}>
          <View style={[styles.flex3, {flexDirection: 'column', justifyContent: 'space-between', alignItems:'center', marginTop: 30}]}>
            <Text style = {styles.bigblue}>Loi Giai Chu</Text>
            <LoiGiai></LoiGiai>
          </View>
          <Buttons 
            yCau = {this.state.yCau} 
            resetState= {()=> {this._setMainVisible(true)}}
            boiState = {()=> {}}>
          </Buttons> 
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 100,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  flex1: {
    flex: 1
  }, 
  flex2: {
    flex: 2
  }, 
  flex3: {
    flex: 3
  },
  bigblue: {
    color: '#066008',
    fontWeight: 'bold',
    fontSize: 30,
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0,0,0,0.65)',
  },
  toolbar: {
    backgroundColor: '#f1f1f1',
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  toolbarRight: {
    alignSelf: 'flex-end',
  },
});


AppRegistry.registerComponent("App", () => App);